package main

import (
	"bytes"
	"fmt"

	"net/http"

	"encoding/json"

	"strings"

	"io/ioutil"

	"regexp"
)

type Appimage struct {
	Version int `json:"version"`

	HomePageURL string `json:"home_page_url"`

	FeedURL string `json:"feed_url"`

	Description string `json:"description"`

	Icon string `json:"icon"`

	Favicon string `json:"favicon"`

	Expired bool `json:"expired"`

	Items []struct {
		Name string `json:"name"`

		Description string `json:"description,omitempty"`

		Categories []string `json:"categories"`

		Authors []struct {
			Name string `json:"name"`

			URL string `json:"url"`
		} `json:"authors"`

		License interface{} `json:"license"`

		Links []Link `json:"links"`

		Icons []string `json:"icons"`

		Screenshots []string `json:"screenshots"`
	} `json:"items"`
}

type Link struct {
	Type string `json:"type"`

	URL string `json:"url"`
}

func checkUrl(url string) string {

	if strings.HasPrefix(url, "https://download.opensuse.org/") {

		return "opensuse.org"

	}

	if strings.HasPrefix(url, "https://github.com/") {

		return "github.com"

	}

	return url

}

func main() {
	appimage := Appimage{}

	file, err := ioutil.ReadFile("snap-repos/appimage.json")

	if err != nil {
		panic(err)
	}

	reader := bytes.NewReader(file)
	json.NewDecoder(reader).Decode(&appimage)

	for i, app := range appimage.Items {

		for _, url := range app.Links {

			if url.Type == "Download" {

				switch checkUrl(url.URL) {

				case "opensuse.org":

					resp, _ := http.Get(url.URL)

					defer resp.Body.Close()

					body, _ := ioutil.ReadAll(resp.Body)

					resp.Body.Close()

					re := regexp.MustCompile(`\s*(?i)href\s*=\s*(\"([^"]*\")|'[^']*'|([^'">\s]+))`)

					for _, durl := range re.FindAllString(string(body), -1) {

						if strings.Contains(durl, ".AppImage") {

							splitted_url := strings.Split(durl, "\"")[1]

							appimage.Items[i].Links = append(appimage.Items[i].Links, Link{"uas", splitted_url})

							break

						}

					}

				case "github.com":

					resp, _ := http.Get(url.URL)

					defer resp.Body.Close()

					body, _ := ioutil.ReadAll(resp.Body)

					resp.Body.Close()

					re := regexp.MustCompile(`\s*(?i)href\s*=\s*(\"([^"]*\")|'[^']*'|([^'">\s]+))`)

					for _, durl := range re.FindAllString(string(body), -1) {

						if strings.Contains(durl, ".AppImage") {

							splitted_url := strings.Split(durl, "\"")[1]

							if !strings.HasPrefix(splitted_url, "https://github.com") {

								splitted_url = "https://github.com" + splitted_url

							}

							appimage.Items[i].Links = append(appimage.Items[i].Links, Link{"uas", splitted_url})

							break

						}

					}

				default:

					fmt.Println(url.URL)

				}

			}

		}

	}

	file, err = json.MarshalIndent(appimage, "", " ")

	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("snap-repos/appimage.json", file, 0644)

	if err != nil {
		panic(err)
	}
}
